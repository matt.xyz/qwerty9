import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialEscolarComponent } from './material-escolar.component';

describe('MaterialEscolarComponent', () => {
  let component: MaterialEscolarComponent;
  let fixture: ComponentFixture<MaterialEscolarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaterialEscolarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialEscolarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
